package com.example.lecturesecond

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            var number = ""
            if (randomGenerator()){
                 number = "ლუწია"
            }else{
                number = "კენტია"

            }
            Toast.makeText(this, "მიღებული რიცხვი ${number}", Toast.LENGTH_SHORT).show()

        }
    }
    private fun randomGenerator():Boolean{
        val randomNumber = (1..101).random()
        d("randomNumber", randomNumber.toString())
        return randomNumber%2==0



    }

}
